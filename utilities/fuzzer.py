#!/usr/bin/python
# -*- coding: utf-8 -*-
# vim: ai ts=4 sts=4 et sw=4
#
# Copyright © 2015 Collabora Ltd.
#
# This library is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation.
#
# This library is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library. If not, see <http://www.gnu.org/licenses/>.

import random
import sys


def fuzzer(content):
    content = list(content)
    length = len(content)
    start_point = random.randint(0, length - length / 10)
    for i in range(start_point, start_point + length / 10):
        content[i] = unichr(random.randint(0x20, 0x10ffff))
    return "".join(content)

if __name__ == '__main__':
    reload(sys)
    sys.setdefaultencoding("utf8")

    with open(sys.argv[1], "r") as card_file:
        card = card_file.read()

    fuzzed = fuzzer(card)
    print(fuzzed)
