/*
 * vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s:
 *
 * Copyright © 2015 Collabora Ltd.
 *
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library. If not, see <http://www.gnu.org/licenses/>.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>
#include <libebook-contacts/libebook-contacts.h>

int
main (int    argc,
      char  *argv[])
{
  EVCard *e_vcard[argc];
  gint i;

  for (i = 1; i < argc; i++)
    {
      gboolean val;
      gsize length;
      GError *error = NULL;
      gchar *contents;

      val = g_file_get_contents (argv[i], &contents, &length, &error);

      if (val)
        {
          e_vcard[i] = e_vcard_new_from_string (contents);
          g_free (contents);
        }
      else
        {
          g_printerr ("Error: could not pass vCard number %d at address %s, "
                      "code: %d, domain: %s\n", i, argv[i], error->code,
                      g_quark_to_string s(error->domain));
          g_clear_error (&error);

          return -1;
        }
    }

  for (i = 1; i < argc; i++)
    {
      gchar **length_str;
      const gchar *end_ptr = NULL;
      guint64 expected_length;
      guint strv_length;

      g_printf ("Processing vCard %s\n", argv[i]);
      length_str = g_strsplit (argv[i], ".", -1);
      strv_length = g_strv_length (length_str);

      if (strv_length != 2)
        {
          g_printerr ("blah\n");

          return 1;
        }

      expected_length = g_ascii_strtoull (length_str[1],
                                          (gchar **) &end_ptr, 10);
      if (*end_ptr != '\0')
        {
          g_printerr ("Error: file has not been named correctly\n");

          return -1;
        }

      g_strfreev(length_str);

      GList *attributes = e_vcard_get_attributes (e_vcard[i]);
      guint list_len = g_list_length (attributes);

      if (expected_length != list_len)
        {
          g_printerr ("Error: expected length of attributes list does not "
                      "match actual length for vCard %s\n", argv[i]);
        }

      e_vcard_dump_structure (e_vcard[i]);
      g_object_unref (e_vcard[i]);
    }

  return 0;
}
