# Work out our current release version
version=$(shell git describe --abbrev=4 --match='v*' HEAD 2>/dev/null || git describe --abbrev=4 HEAD 2>/dev/null)

# Release tarball generation
dist: check vcard-test-suite-$(version).tar.gz

vcard-test-suite-$(version).tar.gz:
	git archive --prefix=vcard-test-suite-$(version)/ $(version) --output $@

# Check that all vCards are named appropriately
check-naming:
	status=0; \
	for i in vcards/valid/* vcards/invalid/*; do \
		case "$$i" in \
			*.[0-9]*.vcf) : ;; \
			*) echo "Invalid vCard name: $$i" >&2; status=1 ;; \
		esac \
	done; \
	exit $$status

# Check that no two example vCards are the same
check-uniqueness:
	@dups="`sha256sum vcards/valid/* vcards/invalid/* | cut -d ' ' -f1 | sort | uniq -d`"; \
	if [ "$$dups" != "" ]; then \
		echo -e "Duplicate vCards with sha256sums:\n$$dups" >&2; \
		exit 1; \
	fi

check: check-naming check-uniqueness

.PHONY: dist check check-naming check-uniqueness
