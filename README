vCard test suite
================

The vCard test suite is a collection of valid and invalid vCards to be used for
testing vCard parsers and code which processes vCards.

Additions of new vCards are very welcome.

Using vCard test suite
======================

The test suite is provided simply as a collection of vCards. Consumers of it are
expected to write their own test harness which feeds each of the vCards into the
code under test, which will typically be a vCard parser; that parser may then
pass its results to other code under test, such as a UI.

vCards are categorised as:
 • Valid: They conform to the specification, although the contact information
   they represent may be nonsense.
 • Invalid: They contravene the vCard specification.

It is up to the code under test whether it does a ‘best effort’ at parsing the
invalid vCards; or whether it errors out.

The vCard files follow a standard naming scheme:
   [name].[property count].vcf

The name can be anything, and has no special significance. The property count
gives the number of vCard properties which should be extractable from the vCard.
For invalid cards, this is the number which should be extractable if the parser
ignores properties which contravene the specification, but tries to parse
others.

Licensing
=========

vCard test suite is licensed under the LGPL; see COPYING for more details.

Bugs
====

Bug reports and (git formatted) patches should be e-mailed to one of the
addresses listed in the ‘Contact’ section below. Thank you!

Contact
=======

Philip Withnall <philip.withnall@collabora.co.uk>
Alex Shtyrov <alex.shtyrov@collabora.co.uk>
http://people.collabora.com/~pwith/vcard-test-suite/
